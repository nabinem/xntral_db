-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 08, 2016 at 12:20 पूर्वाह्न
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xntral`
--

-- --------------------------------------------------------

--
-- Table structure for table `patient_nyha`
--

CREATE TABLE `patient_nyha` (
  `pn_id` int(11) UNSIGNED NOT NULL,
  `pn_class` tinyint(2) NOT NULL COMMENT '1 => class I ,  2 => class II, 3 => class III, 4 =>  class IV   ',
  `pn_patient_id` int(11) NOT NULL,
  `pn_user_id` int(11) NOT NULL,
  `pn_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pn_modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `patient_nyha`
--
ALTER TABLE `patient_nyha`
  ADD PRIMARY KEY (`pn_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `patient_nyha`
--
ALTER TABLE `patient_nyha`
  MODIFY `pn_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
