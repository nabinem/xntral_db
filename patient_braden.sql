-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 08, 2016 at 01:14 पूर्वाह्न
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xntral`
--

-- --------------------------------------------------------

--
-- Table structure for table `patient_braden`
--

CREATE TABLE `patient_braden` (
  `pb_id` int(11) UNSIGNED NOT NULL,
  `pb_sensory_perception` tinyint(2) NOT NULL COMMENT '1 => Completely Limited , 2 => Very Limited , 3 => Slightly Limited , 4 => No Impairment',
  `pb_integumentary_moisture` tinyint(2) NOT NULL COMMENT '1 => Constantly Moist , 2 => Often Moist , 3 => Occasionally Moist , 4 => Rarely Moist',
  `pb_integumentary_activity` tinyint(2) NOT NULL COMMENT '1 => Bedfast , 2 => Chairfast , 3 => Walks Occasionally , 4 => Walks Frequently',
  `pb_integumentary_mobility` tinyint(2) NOT NULL COMMENT '1 => Completely Immobile 2 => Very Limited 3 => Slightly Limited , 4 => No Limitation',
  `pb_integumentary_nutrition` tinyint(2) NOT NULL COMMENT '1 => Very Poor , 2 => Probably Inadequate , 3 => Adequate , 4 => Excellent',
  `pb_integumentary_friction_shear` tinyint(2) NOT NULL COMMENT '1 => Problem , 2 => Potential Problem 3 => No Apparent Problem',
  `pb_total` int(11) NOT NULL COMMENT '19 or above => Not at Risk, 15-18 => At risk, 13-14 => Moderate risk , 10-12 => High risk , 9 or below => Very high risk',
  `pb_patient_id` int(11) NOT NULL,
  `pb_user_id` int(11) NOT NULL,
  `pb_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pb_modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `patient_braden`
--
ALTER TABLE `patient_braden`
  ADD PRIMARY KEY (`pb_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `patient_braden`
--
ALTER TABLE `patient_braden`
  MODIFY `pb_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
